package de.Barschnacken.Codes;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class Code implements ConfigurationSerializable {

	private int anzahl;
	private String nutzbarkeit;
	private boolean gueltig;
	
	public Code(int anzahl, String nutzbarkeit, boolean gueltig){
		this.anzahl = anzahl;
		this.nutzbarkeit = nutzbarkeit;
		this.gueltig = gueltig;
	}
	
	@Override
	public Map<String, Object> serialize(){
		Map<String, Object> o = new HashMap<String, Object>();
		
		o.put("anzahl", this.anzahl);
		o.put("nutzbarkeit", this.nutzbarkeit);
		o.put("gueltig", this.gueltig);
		
		return o;
	}

	public int getAnzahl() {
		return anzahl;
	}
	
	public boolean getGueltig(){
		return gueltig;
	}
	
	public void setGueltig(boolean gueltig) {
		this.gueltig = gueltig;
	}

	public String getNutzbarkeit() {
		return nutzbarkeit;
	}
	
	public void setNutzbarkeit(String nutzbarkeit) {
		this.nutzbarkeit = nutzbarkeit;
	}
	
}
