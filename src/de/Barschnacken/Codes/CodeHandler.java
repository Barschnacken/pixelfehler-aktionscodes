package de.Barschnacken.Codes;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class CodeHandler {

	private File code_database = new File("plugins/Aktionscodes", "codes.yml");
	private FileConfiguration config_code_db = YamlConfiguration.loadConfiguration(code_database);
	
	private HashMap<String, Code> codesRuntime = new HashMap<String, Code>();
	
	public CodeHandler(){
		loadCodes();
	}
	
	public void loadCodes(){
		for(String code : this.config_code_db.getConfigurationSection("").getKeys(false)){
			int anzahl = this.config_code_db.getInt(code + ".anzahl");
			String nutzbarkeit = this.config_code_db.getString(code + ".nutzbarkeit");
			boolean gueltig = this.config_code_db.getBoolean(code + ".gueltig");
			
			this.codesRuntime.put(code, new Code(anzahl, nutzbarkeit, gueltig));
		}
	}
	
	public void saveCodes(){
		for(String id : this.codesRuntime.keySet()){
			this.config_code_db.set(id, this.codesRuntime.get(id).serialize());
		}
		
		try{
			this.config_code_db.save(code_database);
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void addCode(String code, String nutzbarkeit, int anzahl_tokens){
		this.codesRuntime.put(code, new Code(anzahl_tokens, nutzbarkeit, true));
		this.saveCodes();
	}
	
	public void deleteCode(String code){
		this.codesRuntime.remove(code);
		this.saveCodes();
	}
	
	public boolean checkCodeIsValid(String code){
		if(this.codesRuntime.containsKey(code) && this.codesRuntime.get(code).getGueltig() == true){
			return true;
		} else {
			return false;
		}
	}
	
	public Code getCode(String code){
		return this.codesRuntime.get(code);
	}
	
	public boolean checkCodeExist(String code){
		if(this.codesRuntime.containsKey(code)){
			return true;
		} else {
			return false;
		}
	}
	
	public HashMap<String, Code> getCodesRuntime() {
		return codesRuntime;
	}
}
