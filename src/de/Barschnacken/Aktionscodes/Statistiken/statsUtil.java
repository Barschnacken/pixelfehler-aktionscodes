package de.Barschnacken.Aktionscodes.Statistiken;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class statsUtil {

	public static void createStats(String code){
		File statsFile = new File("plugins/Aktionscodes/Statistiken", code + ".yml");
		FileConfiguration cfgStats = YamlConfiguration.loadConfiguration(statsFile);
		
		cfgStats.set("anzahl_genutzt", 0);
		
		try {
			cfgStats.save(statsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> getStatsList(){
		ArrayList<String> stats = new ArrayList<String>();
		
		File statsFolder = new File("plugins/Aktionscodes/Statistiken");
		
		for(final File file:statsFolder.listFiles()){
			stats.add(file.getName().replace(".yml", ""));
		}
		
		return stats;
	}
	
	public static boolean statsExists(String code){
		File statsFile = new File("plugins/Aktionscodes/Statistiken", code + ".yml");
		
		return statsFile.exists();
	}
	
	public static int getUsage(String code){
		int usage = 0;
		
		File statsFile = new File("plugins/Aktionscodes/Statistiken", code + ".yml");
		FileConfiguration cfgStats = YamlConfiguration.loadConfiguration(statsFile);
		
		if(statsFile != null && cfgStats != null){
			usage = cfgStats.getInt("anzahl_genutzt");
		}
		
		return usage;
	}
	
	public static void updateStatsAfterUse(String code, String playerName){
		File statsFile = new File("plugins/Aktionscodes/Statistiken", code + ".yml");
		FileConfiguration cfgStats = YamlConfiguration.loadConfiguration(statsFile);
		
		cfgStats.set("anzahl_genutzt", (cfgStats.getInt("anzahl_genutzt") + 1));
		cfgStats.set(playerName, 1);
		
		try {
			cfgStats.save(statsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean checkIfUserHasUsed(String code, String playerName){
		File statsFile = new File("plugins/Aktionscodes/Statistiken", code + ".yml");
		FileConfiguration cfgStats = YamlConfiguration.loadConfiguration(statsFile);
		
		if(cfgStats.getInt(playerName) == 1){
			return true;
		}
		
		return false;
	}
	
}
