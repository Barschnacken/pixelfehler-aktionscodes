package de.Barschnacken.Aktionscodes;

import org.bukkit.plugin.java.JavaPlugin;

import de.Barschnacken.Aktionscodes.Commands.createcodeCommand;
import de.Barschnacken.Aktionscodes.Commands.deletecodeCommand;
import de.Barschnacken.Aktionscodes.Commands.listcodesCommand;
import de.Barschnacken.Aktionscodes.Commands.statsCommand;
import de.Barschnacken.Aktionscodes.Commands.usecodeCommand;
import de.Barschnacken.Codes.CodeHandler;

public class Main extends JavaPlugin {

	@SuppressWarnings("unused")
	private static Main plugin;
	
	public static CodeHandler code_handler = new CodeHandler();
	
	@Override
	public void onEnable(){
		Main.plugin = this;
		
		//Register commands
		this.getCommand("createcode").setExecutor(new createcodeCommand());
		this.getCommand("code").setExecutor(new usecodeCommand());
		this.getCommand("listcodes").setExecutor(new listcodesCommand());
		this.getCommand("deletecode").setExecutor(new deletecodeCommand());
		this.getCommand("codestats").setExecutor(new statsCommand());
		
		System.out.println("===== Aktionscode's System wurde erfolgreich geladen! =====");
	}
	
	@Override
	public void onDisable(){
		code_handler.saveCodes();
		System.out.println("===== Aktionscode's System wurde erfolgreich gestoppt! =====");
	}
	
}