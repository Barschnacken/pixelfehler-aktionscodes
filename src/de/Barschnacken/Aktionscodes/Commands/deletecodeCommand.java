package de.Barschnacken.Aktionscodes.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Barschnacken.Aktionscodes.Main;

public class deletecodeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(!cs.hasPermission("Aktionscode.Admin")){
			cs.sendMessage("�cDu hast nicht die n�tige Berechtigung um diesen Befehl zu verwenden!");
			return true;
		}
		
		if(args.length < 1){
			cs.sendMessage("�cBitte benutze: /deletecode <Code>");
			return true;
		}
		
		String code = args[0].toLowerCase();
		
		if(!Main.code_handler.checkCodeExist(code)){
			cs.sendMessage("�cDieser Code existiert nicht und kann daher auch nicht gel�scht werden.");
			return true;
		}
		
		Main.code_handler.deleteCode(args[0].toLowerCase());
		cs.sendMessage("�2Der Code �6" + code + "�2 wurde erfolgreich gel�scht! Die Statistiken sind weiterhin verf�gbar bis diese seperat gel�scht werden!");
		
		return true;
	}
	
}
