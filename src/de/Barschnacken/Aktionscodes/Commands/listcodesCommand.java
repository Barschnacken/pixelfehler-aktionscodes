package de.Barschnacken.Aktionscodes.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Barschnacken.Aktionscodes.Main;
import de.Barschnacken.Codes.Code;

public class listcodesCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(!cs.hasPermission("Aktionscode.Admin")){
			cs.sendMessage("�cDu hast nicht die n�tige Berechtigung um diesen Befehl zu verwenden!");
			return true;
		}
		
		cs.sendMessage("�6=== G�ltige Codes ===");
		
		for(String code:Main.code_handler.getCodesRuntime().keySet()){
			Code codeObj = Main.code_handler.getCode(code);
			
			if(codeObj.getGueltig() == true){
				cs.sendMessage("�f- �6" + code + "�f | �6" + codeObj.getAnzahl());
			}
		}
		
		return true;
	}
}
