package de.Barschnacken.Aktionscodes.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Barschnacken.Aktionscodes.Main;
import de.Barschnacken.Aktionscodes.Statistiken.statsUtil;

public class createcodeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(!cs.hasPermission("Aktionscode.Admin")){
			cs.sendMessage("�cDu hast nicht die n�tige Berechtigung um diesen Befehl zu verwenden!");
			return true;
		}
		
		if(args.length < 3){
			cs.sendMessage("�cBitte benutze: /createcode <Code> <e/m> <Anzahl der Tokens>");
			cs.sendMessage("�6Hinweise zu diesem Befehl:");
			cs.sendMessage("�6- �fDie Angabe e steht f�r einfach verwendbar. D.h. der Code kann einmalig von einem User verwendet werden und wird dann gel�scht.");
			cs.sendMessage("�6- �fDie Angabe m steht f�r mehrfach verwendbar. D.h. der Code kann bis er durch ein Teammitglied deaktiviert/gel�scht wird, von jedem User 1 mal verwendet werden.");
			return true;
		}
		
		if(!args[1].equalsIgnoreCase("e") && !args[1].equalsIgnoreCase("m")){
			cs.sendMessage("�cBitte gebe f�r die G�ltigkeit des Codes den Buchstaben 'e' oder 'm' ein!");
			return true;
		}
		
		String code = args[0].toLowerCase();
		String nutzbarkeit = args[1];
		int anzahl;
		
		try {
			anzahl = Integer.parseInt(args[2]);
		} catch(NumberFormatException e){
			cs.sendMessage("�cAls 'Anzahl der Tokens' muss einen Zahl angegeben werden!");
			return true;
		}
		
		if(Main.code_handler.checkCodeExist(code)){
			cs.sendMessage("�cDieser Code existiert schon, bitte w�hle einen anderen!");
			return true;
		}
		
		Main.code_handler.addCode(code, nutzbarkeit, anzahl);
		statsUtil.createStats(code);
		
		String responseString = "�2Du hast erfolgreich den Code �f'" + args[0] + "'�2 angelegt. Dieser gibt dem Benutzer �f" + anzahl + " Tokens.�2 ";
		
		if(nutzbarkeit.equalsIgnoreCase("e")){
			responseString = responseString + " Dieser kann nur von einem Benutzer verwendet werden und wird dann automatisch gel�scht!";
		} else if(nutzbarkeit.equalsIgnoreCase("m")){
			responseString = responseString + " Dieser kann von jedem Benutzer ein mal verwendet werden, bis der Code manuell gel�scht wird!";
		}
		
		cs.sendMessage(responseString);
		return true;
	}
	
}
