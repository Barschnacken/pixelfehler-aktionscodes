package de.Barschnacken.Aktionscodes.Commands;

import java.util.ArrayList;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Barschnacken.Aktionscodes.Statistiken.statsUtil;

public class statsCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(!cs.hasPermission("Aktionscode.Admin")){
			cs.sendMessage("�cDu hast nicht die n�tige Berechtigung um diesen Befehl zu verwenden!");
			return true;
		}
		
		if(args.length < 1){
			cs.sendMessage("�cBenutze: /codestats <show/list> (Code)");
			return true;
		}
		
		if(args[0].equalsIgnoreCase("list")){
			ArrayList<String> statsList = statsUtil.getStatsList();
			
			cs.sendMessage("�6===�f Verf�gbare Statistiken �6===");
		
			for(String statName:statsList){
				cs.sendMessage("�f- " + statName);
			}
		} else if(args[0].equalsIgnoreCase("show")){
			if(args.length < 2){
				cs.sendMessage("�cBei diesem Befehl musst du den zu �berpr�fenden Code angeben.");
				return true;
			}
			
			if(!statsUtil.statsExists(args[1].toLowerCase())){
				cs.sendMessage("�cZu dem angegebenen Code existieren keine Statistiken!");
				return true;
			}
			
			cs.sendMessage("�fDer Code �6" + args[1] + "�f wurde �6" + statsUtil.getUsage(args[1].toLowerCase()) + " mal �f benutzt.");
		} else {
			cs.sendMessage("�cBenutze: /codestats <show/list> (Code)");
		}
		
		return true;
	}
	
}
