package de.Barschnacken.Aktionscodes.Commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import de.Barschnacken.Aktionscodes.Main;
import de.Barschnacken.Aktionscodes.Statistiken.statsUtil;
import de.Barschnacken.Codes.Code;

public class usecodeCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(args.length < 1){
			cs.sendMessage("�cBitte gebe /code <Code> ein.");
			return true;
		}
		
		String code = args[0].toLowerCase();
		
		if(Main.code_handler.checkCodeIsValid(code)){
			Code usedCode = Main.code_handler.getCode(code);
			
			if(statsUtil.checkIfUserHasUsed(code, cs.getName())){
				cs.sendMessage("�cDu kannst diesen Code nur einmal einl�sen!");
				return true;
			}
			
			String command = "tokens addtokens " + cs.getName() + " " + usedCode.getAnzahl();
			//Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
			Bukkit.broadcastMessage(command);
			
			if(usedCode.getNutzbarkeit().equalsIgnoreCase("e")){
				Main.code_handler.deleteCode(code);
			}
			
			cs.sendMessage("�2Du hast erfolgreich den Code $f" + code + "�2 eingel�st und daf�r �f" + usedCode.getAnzahl() + " Tokens �2erhalten.");
			
			statsUtil.updateStatsAfterUse(code, cs.getName());
		} else {
			cs.sendMessage("�cLeider existiert dieser Code nicht oder ist nicht mehr g�ltig!");
			return true;
		}
		
		return true;
	}
}
